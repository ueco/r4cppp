fn main() {
    let x: bool = true;
    let x = 34; // isize
    let x = 34u32; // usize
    let x: u8 = 34u8;
    let x = 34i64;
    let x = 34f32;

    let x = 0b1100;
    let x = 0o14; // octal
    let x = 0xe;
    let x = 0b_1100_0110_0101_0001;
}

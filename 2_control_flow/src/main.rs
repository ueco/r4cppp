fn foo(x: i32) -> &'static str {
    let mut result: &'static str;
    if x < 10 {
        result = "less then 10";
    } else {
        result = "10 or more";
    }
    return result;
}

fn bar(x: i32) -> &'static str {
    if x < 10 {
        "less then 10"
    } else {
        "10 or more"
    }
}

fn print_vector(vec: Vec<i32>) {
    for i in 0..vec.len() {
        println!("{}: {}", i, vec[i]);
    }
}

fn main() {
    println!("{}", bar(5));
    println!("{}", foo(11));

    let mut x = 3;
    while x > 0 {
        println!("Current value: {}", x);
        x -= 1;
    }

    let vec = vec![1, 2, 2, 2, 3];
    print_vector(vec);
}

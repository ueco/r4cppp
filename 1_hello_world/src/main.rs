fn show_me_the(_x: &'static str) -> &'static str {
    "world?"
}

fn main() {
    let world = "world!";
    let specific_world: &'static str = "world...";
    println!("Hello, {}", world);
    println!("Hello, {}", specific_world);
    println!("Hello, {}", show_me_the("crap"));
}
